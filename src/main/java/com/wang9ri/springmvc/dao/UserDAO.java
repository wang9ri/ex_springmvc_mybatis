package com.wang9ri.springmvc.dao;

import java.util.ArrayList;
import org.apache.ibatis.session.SqlSession;

import com.wang9ri.springmvc.vo.UserVO;

public class UserDAO {

	//SqlSession는  mybatis-context에서 사용됨.
	private SqlSession sqlSession;

	public void setSqlSession(SqlSession sqlSession) {

		this.sqlSession = sqlSession;
	}

	public UserVO selectUser() {
		
		return (UserVO)sqlSession.selectOne("userVO.selectUser");
	}

	public ArrayList<UserVO> selectUsers() {
		
		return (ArrayList<UserVO>) sqlSession.selectList("userVO.selectUsers");
	}	
}
